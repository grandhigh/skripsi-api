'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class QuestionnaireScaleSetup extends Model {
	static async StoreScale(scaleCode, scaleInfo) {
		const data = await this.create({ ...scaleInfo, scale_code: scaleCode})
		return { success: true, data }
	}
	static async ChangeScale(scaleCode, scaleId, scaleInfo) {
		const data = await this.query().where('scale_code', scaleCode).where('id', scaleId).update(scaleInfo)
		return { success: true, data }
	}
	static async RemoveScale(scaleCode, scaleId) {
		const data = await this.query().where('scale_code', scaleCode).where('id', scaleId).delete()
		return { success: true, data }
	}
}

module.exports = QuestionnaireScaleSetup
