'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class QuestionnaireSite extends Model {
	static async ListOfSurvey(userId) {
		const data = await this.query().where('created_by', userId).fetch()
		return { success: true, data }
	}
	static async StoreSurvey(userId, surveyInfo) {
		const data = await this.create({ ...surveyInfo, created_by: userId})
		return { success: true, data }
	}
	static async ChangeSurvey(userId, surveyId, surveyInfo) {
		const data = await this.query().where('created_by', userId).where('id', surveyId).update(surveyInfo)
		return { success: true, data }
	}
	static async RemoveSurvey(userId, surveyId) {
		const data = await this.query().where('created_by', userId).where('id', surveyId).delete()
		return { success: true, data }
	}
}

module.exports = QuestionnaireSite
