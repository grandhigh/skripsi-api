'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')

class QuestionnaireScale extends Model {
	static get primaryKey () {
		return 'code'
	}

	static async ListOfScale(userId) {
		const details = await this.query()
			.select(Database.raw('questionnaire_scales.code,questionnaire_scales.name, ARRAY_TO_JSON(ARRAY_AGG(questionnaire_scale_setups)) scales'))
		.where('created_by', userId)
		.leftJoin('questionnaire_scale_setups', 'questionnaire_scales.code', 'questionnaire_scale_setups.scale_code')
		.groupBy(Database.raw('1, 2'))
		.fetch()
		const data = []
		const scale = []
		for (const idx in details) {
			const item = details[idx]
		}
		return { success: true, data: details }
	}
	static async StoreScale(userId, scaleInfo) {
		const data = await this.create({ ...scaleInfo, created_by: userId})
		return { success: true, data }
	}
	static async ChangeScale(userId, scaleId, scaleInfo) {
		const data = await this.query().where('created_by', userId).where('code', scaleId).update(scaleInfo)
		return { success: true, data }
	}
	static async RemoveScale(userId, scaleId) {
		const data = await this.query().where('created_by', userId).where('code', scaleId).delete()
		return { success: true, data }
	}
}

module.exports = QuestionnaireScale
