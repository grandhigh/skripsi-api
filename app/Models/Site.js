'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Site extends Model {
	static async ListOfSite(userId) {
		const data = await this.query().where('created_by', userId).fetch()
		return { success: true, data }
	}
	static async StoreSite(userId, siteInfo) {
		const data = await this.create({ ...siteInfo, created_by: userId})
		return { success: true, data }
	}
	static async ChangeSite(userId, siteId, siteInfo) {
		const data = await this.query().where('created_by', userId).where('id', siteId).update(siteInfo)
		return { success: true, data }
	}
	static async RemoveSite(userId, siteId) {
		const data = await this.query().where('created_by', userId).where('id', siteId).delete()
		return { success: true, data }
	}
}

module.exports = Site
