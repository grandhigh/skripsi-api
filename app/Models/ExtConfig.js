'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ExtConfig extends Model {
}

module.exports = ExtConfig
