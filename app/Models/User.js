'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static async Login (username, password) {
    try {
      const data = await this.query().where('username', username).fetch()
      if (data.rows.length === 0) throw Error('Pengguna tidak ditemukan')
      const info = data.rows[0]
      const isSame = await Hash.verify(password, info.password)
      if (!isSame) throw Error('Kata sandi tidak benar')
      return { success: true, data: info }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }
}

module.exports = User
