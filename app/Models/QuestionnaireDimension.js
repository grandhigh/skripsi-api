'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class QuestionnaireDimension extends Model {
	static async StoreQuestionnaire(questionnaireCode, questionnaireInfo) {
		const data = await this.create({ ...questionnaireInfo, questionnaire_code: questionnaireCode})
		return { success: true, data }
	}
	static async ChangeQuestionnaire(questionnaireCode, questionnaireId, questionnaireInfo) {
		const data = await this.query().where('questionnaire_code', questionnaireCode).where('id', questionnaireId).update(questionnaireInfo)
		return { success: true, data }
	}
	static async RemoveQuestionnaire(questionnaireCode, questionnaireId) {
		const data = await this.query().where('questionnaire_code', questionnaireCode).where('id', questionnaireId).delete()
		return { success: true, data }
	}
}

module.exports = QuestionnaireDimension
