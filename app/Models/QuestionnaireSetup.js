'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class QuestionnaireSetup extends Model {
	static async StoreQuestionnaire(dimensionId, questionnaireInfo) {
		const data = await this.create({ ...questionnaireInfo, dimension_id: dimensionId})
		return { success: true, data }
	}
	static async ChangeQuestionnaire(dimensionId, questionId, questionnaireInfo) {
		const data = await this.query().where('dimension_id', dimensionId).where('id', questionId).update(questionnaireInfo)
		return { success: true, data }
	}
	static async RemoveQuestionnaire(dimensionId, questionId) {
		const data = await this.query().where('dimension_id', dimensionId).where('id', questionId).delete()
		return { success: true, data }
	}
}

module.exports = QuestionnaireSetup
