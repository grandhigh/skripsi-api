'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')
const Respondent = use('App/Models/Respondent')
const Data = use('App/Models/Datum')
const DataDetail = use('App/Models/DataDetail')
const QuestionnaireSite = use('App/Models/QuestionnaireSite')

class Questionnaire extends Model {
	static get primaryKey () {
		return 'code'
	}
	static async LoginApp(code) {
		const data = await QuestionnaireSite.query().where('code', code).fetch()
		if (!data.rows.length) return { success: false, message: 'Kode undangan tidak valid' }

		const data2 = await QuestionnaireSite.query().where('code', code).whereRaw('NOW() < start_at').fetch()
		if (data2.rows.length) return { success: false, message: 'Survei belum dimulai' }

		const data3 = await QuestionnaireSite.query().where('code', code).whereRaw('NOW() > end_at').fetch()
		if (data3.rows.length) return { success: false, message: 'Survei telah slesai' }
		return { success: true, data: data.rows[0] }
		
	}
	static async ListOfQuestions(code) {
		return Database.raw(`SELECT * FROM get_qeustions('${code}')`)
	}
	static async Store(code, resProfile, resSurvey) {
		const trx = await Database.beginTransaction()
		try {
			delete resProfile.checkbox
			const respondentList = (await Respondent.query().where('email', resProfile.email).fetch()).rows
			let respondent = {}
			if (respondentList.length) {
				respondent = respondentList[0]
			} else {
				respondent = await Respondent.create(resProfile, trx)
			}
			const options = (await QuestionnaireSite.query().where('code', code).fetch()).rows[0]
			const data = await Data.create({
				survey_code: code,
				respondent_id: respondent.id
			}, trx)

			const details = []
			for (const key in resSurvey) {
				const detail = await DataDetail.create({
					data_id: data.id,
					setup_id: key.split('_')[1],
					scale_setup_id: resSurvey[key]
				}, trx)
				details.push(detail)
			}
			const result = { success: true, data: details }
			await trx.commit()
			return result
		} catch (error) {
			/*Rollback Transaction*/
			await trx.rollback()

			/*Throw Error*/
			return { success: false, message: error.message }
		}
	}
	static async LikertScalingMethod(siteId,  questionCode) {
		return Database.raw(`SELECT * FROM get_likert_scale(${siteId}, '${questionCode}', true)`)
	}
	static async ListOfQuestionnaire(userId) {
		const details = await this.query()
			.select(
				Database.raw(`
					questionnaires.code,
					questionnaires.name,
					questionnaires.description,
					questionnaires.scale_code,
					(SELECT ARRAY_TO_JSON(ARRAY_AGG(questionnaires)) FROM (SELECT
						d.id, d.code, d.name, d.order_no, ARRAY_TO_JSON(ARRAY_AGG(qs)) questions
					FROM questionnaire_dimensions d LEFT JOIN questionnaire_setups qs ON d.id = qs.dimension_id WHERE d.questionnaire_code = questionnaires.code
					GROUP BY 1, 2, 3 ORDER BY 1) questionnaires) questionnaires
				`)
			)
		.where('created_by', userId)
		.leftJoin('questionnaire_dimensions', 'questionnaires.code', 'questionnaire_dimensions.questionnaire_code')
		.groupBy(Database.raw('1, 2, 3, 4'))
		.fetch()
		const data = []
		const questionnaire = []
		for (const idx in details) {
			const item = details[idx]
		}
		return { success: true, data: details }
	}
	static async StoreQuestionnaire(userId, questionnaireInfo) {
		questionnaireInfo.scale_code = questionnaireInfo.scale_code.split(' - ')[0]
		const data = await this.create({ ...questionnaireInfo, created_by: userId})
		return { success: true, data }
	}
	static async ChangeQuestionnaire(userId, questionnaireId, questionnaireInfo) {
		questionnaireInfo.scale_code = questionnaireInfo.scale_code.split(' - ')[0]
		const data = await this.query().where('created_by', userId).where('code', questionnaireId).update(questionnaireInfo)
		return { success: true, data }
	}
	static async RemoveQuestionnaire(userId, questionnaireId) {
		const data = await this.query().where('created_by', userId).where('code', questionnaireId).delete()
		return { success: true, data }
	}
}

module.exports = Questionnaire
