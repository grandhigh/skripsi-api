'use strict'

const Survey = use('App/Models/QuestionnaireSite')
class QuestionnaireSiteController {
	async listOfSurvey({ request, response }) {
		const { userId } = request.body
		const data = await Survey.ListOfSurvey(userId)

		return data
	}
	async storeSurvey({ request, response }) {
		const { userId, surveyInfo } = request.body
		const data = await Survey.StoreSurvey(userId, surveyInfo)

		return data
	}
	async changeSurvey({ request, response }) {
		const { userId, surveyId, surveyInfo } = request.body
		const data = await Survey.ChangeSurvey(userId, surveyId, surveyInfo)

		return data
	}
	async removeSurvey({ request, response }) {
		const { userId, surveyId } = request.body
		const data = await Survey.RemoveSurvey(userId, surveyId)

		return data
	}
}

module.exports = QuestionnaireSiteController
