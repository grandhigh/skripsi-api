'use strict'

const User = use('App/Models/User')
class UserController {
	async login({ request, response }) {
		const { username, password } = request.body
		const data = await User.Login(username, password)

		return data
	}
}

module.exports = UserController
