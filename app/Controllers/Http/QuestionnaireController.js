'use strict'

const Questionnaire = use('App/Models/Questionnaire')
const Dimension = use('App/Models/QuestionnaireDimension')
const Setup = use('App/Models/QuestionnaireSetup')
class QuestionnaireController {
	async getQuestions({ request, response }) {
		const { code } = request.body
		const data = await Questionnaire.ListOfQuestions(code)

		return { success: true, data: data.rows }
	}
	async calculate({ request, response }) {
		const { siteId,  code } = request.body
		const data = await Questionnaire.LikertScalingMethod(siteId,  code)

		return { success: true, data: data.rows }
	}
	async loginApp({ request, response }) {
		const { code } = request.body
		const data = await Questionnaire.LoginApp(code)

		return data
	}
	async saveData({ request, response }) {
		const { code, userProfile, userSurvey } = request.body
		const data = await Questionnaire.Store(code, userProfile, userSurvey)

		return data
	}
	async listOfQuestionnaire({ request, response }) {
		const { userId } = request.body
		const data = await Questionnaire.ListOfQuestionnaire(userId)

		return data
	}
	async storeQuestionnaire({ request, response }) {
		const { userId, questionnaireInfo } = request.body
		const data = await Questionnaire.StoreQuestionnaire(userId, questionnaireInfo)

		return data
	}
	async changeQuestionnaire({ request, response }) {
		const { userId, questionnaireId, questionnaireInfo } = request.body
		const data = await Questionnaire.ChangeQuestionnaire(userId, questionnaireId, questionnaireInfo)

		return data
	}
	async removeQuestionnaire({ request, response }) {
		const { userId, questionnaireId } = request.body
		const data = await Questionnaire.RemoveQuestionnaire(userId, questionnaireId)

		return data
	}
	async storeQuestionnaireVariable({ request, response }) {
		const { questionnaireCode, questionnaireInfo } = request.body
		const data = await Dimension.StoreQuestionnaire(questionnaireCode, questionnaireInfo)

		return data
	}
	async changeQuestionnaireVariable({ request, response }) {
		const { questionnaireCode, questionnaireId, questionnaireInfo } = request.body
		const data = await Dimension.ChangeQuestionnaire(questionnaireCode, questionnaireId, questionnaireInfo)

		return data
	}
	async removeQuestionnaireVariable({ request, response }) {
		const { questionnaireCode, questionnaireId } = request.body
		const data = await Dimension.RemoveQuestionnaire(questionnaireCode, questionnaireId)

		return data
	}
	async storeQuestionnaireVariableQuestion({ request, response }) {
		const { dimensionId, questionnaireInfo } = request.body
		const data = await Setup.StoreQuestionnaire(dimensionId, questionnaireInfo)

		return data
	}
	async changeQuestionnaireVariableQuestion({ request, response }) {
		const { dimensionId, questionId, questionnaireInfo } = request.body
		const data = await Setup.ChangeQuestionnaire(dimensionId, questionId, questionnaireInfo)

		return data
	}
	async removeQuestionnaireVariableQuestion({ request, response }) {
		const { dimensionId, questionId } = request.body
		const data = await Setup.RemoveQuestionnaire(dimensionId, questionId)

		return data
	}

}

module.exports = QuestionnaireController
