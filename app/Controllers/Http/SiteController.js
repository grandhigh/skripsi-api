'use strict'

const Site = use('App/Models/Site')
class SiteController {
	async listOfSite({ request, response }) {
		const { userId } = request.body
		const data = await Site.ListOfSite(userId)

		return data
	}
	async storeSite({ request, response }) {
		const { userId, siteInfo } = request.body
		const data = await Site.StoreSite(userId, siteInfo)

		return data
	}
	async changeSite({ request, response }) {
		const { userId, siteId, siteInfo } = request.body
		const data = await Site.ChangeSite(userId, siteId, siteInfo)

		return data
	}
	async removeSite({ request, response }) {
		const { userId, siteId } = request.body
		const data = await Site.RemoveSite(userId, siteId)

		return data
	}
}

module.exports = SiteController
