'use strict'

const Scale = use('App/Models/QuestionnaireScale')
const Setup = use('App/Models/QuestionnaireScaleSetup')
class ScaleController {
	async listOfScale({ request, response }) {
		const { userId } = request.body
		const data = await Scale.ListOfScale(userId)

		return data
	}
	async storeScale({ request, response }) {
		const { userId, scaleInfo } = request.body
		const data = await Scale.StoreScale(userId, scaleInfo)

		return data
	}
	async changeScale({ request, response }) {
		const { userId, scaleId, scaleInfo } = request.body
		const data = await Scale.ChangeScale(userId, scaleId, scaleInfo)

		return data
	}
	async removeScale({ request, response }) {
		const { userId, scaleId } = request.body
		const data = await Scale.RemoveScale(userId, scaleId)

		return data
	}
	async storeScaleVariable({ request, response }) {
		const { scaleCode, scaleInfo } = request.body
		const data = await Setup.StoreScale(scaleCode, scaleInfo)

		return data
	}
	async changeScaleVariable({ request, response }) {
		const { scaleCode, scaleId, scaleInfo } = request.body
		const data = await Setup.ChangeScale(scaleCode, scaleId, scaleInfo)

		return data
	}
	async removeScaleVariable({ request, response }) {
		const { scaleCode, scaleId } = request.body
		const data = await Setup.RemoveScale(scaleCode, scaleId)

		return data
	}
}

module.exports = ScaleController
