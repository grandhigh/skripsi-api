'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/v1', () => {
	return 'Bismillah!'
})

Route.post('/v1', () => {
	return { message: 'Bismillah!' }
})

Route.group('v1', () => {
	Route.post("login", "UserController.login") /** Login User  */

	Route.post("login-app", "QuestionnaireController.loginApp") /** Login App  */
	Route.post("list-of-questions", "QuestionnaireController.getQuestions") /** Get Questions  */
	Route.post("get-chart-data", "QuestionnaireController.calculate") /** Get Questions  */
	Route.post("save-data", "QuestionnaireController.saveData") /** Save Survey  */

	Route.post("list-of-site", "SiteController.listOfSite") /** Get Sites  */
	Route.post("store-site", "SiteController.storeSite") /** Store Site  */
	Route.post("edit-site", "SiteController.changeSite") /** Change Site  */
	Route.post("remove-site", "SiteController.removeSite") /** Remove Site  */

	Route.post("list-of-scale", "ScaleController.listOfScale") /** Get Scales  */
	Route.post("store-scale", "ScaleController.storeScale") /** Store Scale  */
	Route.post("edit-scale", "ScaleController.changeScale") /** Change Scale  */
	Route.post("remove-scale", "ScaleController.removeScale") /** Remove Scale  */

	Route.post("store-scale-variable", "ScaleController.storeScaleVariable") /** Store Variable  */
	Route.post("edit-scale-variable", "ScaleController.changeScaleVariable") /** Change Variable  */
	Route.post("remove-scale-variable", "ScaleController.removeScaleVariable") /** Remove Variable  */

	Route.post("list-of-questionnaire", "QuestionnaireController.listOfQuestionnaire") /** Get Questionnaires  */
	Route.post("store-questionnaire", "QuestionnaireController.storeQuestionnaire") /** Store Questionnaire  */
	Route.post("edit-questionnaire", "QuestionnaireController.changeQuestionnaire") /** Change Questionnaire  */
	Route.post("remove-questionnaire", "QuestionnaireController.removeQuestionnaire") /** Remove Questionnaire  */

	Route.post("store-questionnaire-variable", "QuestionnaireController.storeQuestionnaireVariable") /** Store Variable  */
	Route.post("edit-questionnaire-variable", "QuestionnaireController.changeQuestionnaireVariable") /** Change Variable  */
	Route.post("remove-questionnaire-variable", "QuestionnaireController.removeQuestionnaireVariable") /** Remove Variable  */

	Route.post("store-questionnaire-variable-question", "QuestionnaireController.storeQuestionnaireVariableQuestion") /** Store Question  */
	Route.post("edit-questionnaire-variable-question", "QuestionnaireController.changeQuestionnaireVariableQuestion") /** Change Question  */
	Route.post("remove-questionnaire-variable-question", "QuestionnaireController.removeQuestionnaireVariableQuestion") /** Remove Question  */

	Route.post("list-of-survey", "QuestionnaireSiteController.listOfSurvey") /** Get Surveys  */
	Route.post("store-survey", "QuestionnaireSiteController.storeSurvey") /** Store Survey  */
	Route.post("edit-survey", "QuestionnaireSiteController.changeSurvey") /** Change Survey  */
	Route.post("remove-survey", "QuestionnaireSiteController.removeSurvey") /** Remove Survey  */
}).prefix('v1')

Route.get('/api/v1', () => {
	return 'Bismillah!'
})

Route.post('/api/v1', () => {
	return { message: 'Bismillah!' }
})

Route.group('api/v1', () => {
	Route.post("login", "UserController.login") /** Login User  */

	Route.post("login-app", "QuestionnaireController.loginApp") /** Login App  */
	Route.post("list-of-questions", "QuestionnaireController.getQuestions") /** Get Questions  */
	Route.post("get-chart-data", "QuestionnaireController.calculate") /** Get Questions  */
	Route.post("save-data", "QuestionnaireController.saveData") /** Save Survey  */

	Route.post("list-of-site", "SiteController.listOfSite") /** Get Sites  */
	Route.post("store-site", "SiteController.storeSite") /** Store Site  */
	Route.post("edit-site", "SiteController.changeSite") /** Change Site  */
	Route.post("remove-site", "SiteController.removeSite") /** Remove Site  */

	Route.post("list-of-scale", "ScaleController.listOfScale") /** Get Scales  */
	Route.post("store-scale", "ScaleController.storeScale") /** Store Scale  */
	Route.post("edit-scale", "ScaleController.changeScale") /** Change Scale  */
	Route.post("remove-scale", "ScaleController.removeScale") /** Remove Scale  */

	Route.post("store-scale-variable", "ScaleController.storeScaleVariable") /** Store Variable  */
	Route.post("edit-scale-variable", "ScaleController.changeScaleVariable") /** Change Variable  */
	Route.post("remove-scale-variable", "ScaleController.removeScaleVariable") /** Remove Variable  */

	Route.post("list-of-questionnaire", "QuestionnaireController.listOfQuestionnaire") /** Get Questionnaires  */
	Route.post("store-questionnaire", "QuestionnaireController.storeQuestionnaire") /** Store Questionnaire  */
	Route.post("edit-questionnaire", "QuestionnaireController.changeQuestionnaire") /** Change Questionnaire  */
	Route.post("remove-questionnaire", "QuestionnaireController.removeQuestionnaire") /** Remove Questionnaire  */

	Route.post("store-questionnaire-variable", "QuestionnaireController.storeQuestionnaireVariable") /** Store Variable  */
	Route.post("edit-questionnaire-variable", "QuestionnaireController.changeQuestionnaireVariable") /** Change Variable  */
	Route.post("remove-questionnaire-variable", "QuestionnaireController.removeQuestionnaireVariable") /** Remove Variable  */

	Route.post("store-questionnaire-variable-question", "QuestionnaireController.storeQuestionnaireVariableQuestion") /** Store Question  */
	Route.post("edit-questionnaire-variable-question", "QuestionnaireController.changeQuestionnaireVariableQuestion") /** Change Question  */
	Route.post("remove-questionnaire-variable-question", "QuestionnaireController.removeQuestionnaireVariableQuestion") /** Remove Question  */

	Route.post("list-of-survey", "QuestionnaireSiteController.listOfSurvey") /** Get Surveys  */
	Route.post("store-survey", "QuestionnaireSiteController.storeSurvey") /** Store Survey  */
	Route.post("edit-survey", "QuestionnaireSiteController.changeSurvey") /** Change Survey  */
	Route.post("remove-survey", "QuestionnaireSiteController.removeSurvey") /** Remove Survey  */
}).prefix('api/v1')
