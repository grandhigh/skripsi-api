'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RespondentSchema extends Schema {
  up () {
    this.create('respondents', (table) => {
      table.increments()
      table.string('fullname', 80).notNullable()
      table.integer('age').notNullable()
      table.string('gender', 10).notNullable()
      table.string('phone', 16).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('respondents')
  }
}

module.exports = RespondentSchema
