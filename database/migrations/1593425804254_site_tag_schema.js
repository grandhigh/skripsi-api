'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SiteTagSchema extends Schema {
  up () {
    this.create('site_tags', (table) => {
      table.increments()
      table.integer('site_id').unsigned().references('id').inTable('sites')
      table.integer('tag_id').unsigned().references('id').inTable('tags')
      table.timestamps()
    })
  }

  down () {
    this.drop('site_tags')
  }
}

module.exports = SiteTagSchema
