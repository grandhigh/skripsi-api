'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SiteSchema extends Schema {
	up () {
		this.create('sites', (table) => {
			table.increments()
			table.string('name', 80).notNullable()
			table.string('url', 100).notNullable()
			table.string('description', 254).notNullable()
			table.integer('created_by').unsigned().references('id').inTable('users')
			table.timestamps()
		})
	}

	down () {
		this.drop('sites')
	}
}

module.exports = SiteSchema
