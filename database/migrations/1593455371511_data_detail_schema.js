'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataDetailSchema extends Schema {
  up () {
    this.create('data_details', (table) => {
      table.increments()
      table.integer('data_id').unsigned().references('id').inTable('data')
      table.integer('setup_id').unsigned().references('id').inTable('questionnaire_setups')
      table.integer('scale_setup_id').unsigned().references('id').inTable('questionnaire_scale_setups')
      table.timestamps()
    })
  }

  down () {
    this.drop('data_details')
  }
}

module.exports = DataDetailSchema
