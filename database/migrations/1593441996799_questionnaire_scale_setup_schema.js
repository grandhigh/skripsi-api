'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireScaleSetupSchema extends Schema {
  up () {
    this.create('questionnaire_scale_setups', (table) => {
      table.increments()
      table.string('scale_code').unsigned().references('code').inTable('questionnaire_scales')
      table.string('name')
      table.integer('value')
      table.integer('order_no')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaire_scale_setups')
  }
}

module.exports = QuestionnaireScaleSetupSchema
