'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExtConfigSchema extends Schema {
  up () {
    this.create('ext_configs', (table) => {
      table.increments()
      table.string('code', 80).notNullable().unique()
      table.string('value', 80).notNullable().unique()
      table.string('description', 80).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('ext_configs')
  }
}

module.exports = ExtConfigSchema
