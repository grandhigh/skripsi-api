'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataSchema extends Schema {
  up () {
    this.create('data', (table) => {
      table.increments()
      table.integer('site_id').unsigned().references('id').inTable('sites')
      table.string('questionnaire_code').unsigned().references('code').inTable('questionnaires')
      table.integer('respondent_id').unsigned().references('id').inTable('respondents')
      table.timestamps()
    })
  }

  down () {
    this.drop('data')
  }
}

module.exports = DataSchema
