'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArticleSchema extends Schema {
  up () {
    this.create('articles', (table) => {
      table.increments()
      table.integer('site_id').unsigned().references('id').inTable('sites')
      table.string('name', 80).notNullable()
	  table.string('url', 100).notNullable()
      table.string('description', 254).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('articles')
  }
}

module.exports = ArticleSchema
