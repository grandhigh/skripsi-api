'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireSetupSchema extends Schema {
  up () {
    this.create('questionnaire_setups', (table) => {
      table.increments()
      table.integer('dimension_id').unsigned().references('id').inTable('questionnaire_dimensions')
      table.string('code', 10).notNullable()
      table.string('question', 700).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaire_setups')
  }
}

module.exports = QuestionnaireSetupSchema
