'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireScaleSchema extends Schema {
  up () {
    this.create('questionnaire_scales', (table) => {
      table.string('code', 10).notNullable().unique()
      table.string('name', 80).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaire_scales')
  }
}

module.exports = QuestionnaireScaleSchema
