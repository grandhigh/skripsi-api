'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireDimensionSchema extends Schema {
  up () {
    this.create('questionnaire_dimensions', (table) => {
      table.increments()
      table.string('questionnaire_code').unsigned().references('code').inTable('questionnaires')
      table.string('code', 10).notNullable()
      table.string('name', 80).notNullable()
      table.integer('order_no').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaire_dimensions')
  }
}

module.exports = QuestionnaireDimensionSchema
