'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireSiteSchema extends Schema {
  up () {
    this.create('questionnaire_sites', (table) => {
      table.increments()
      table.string('code', 10).notNullable().unique()
      table.string('name', 80).notNullable()
      table.string('questionnaire_code').unsigned().references('code').inTable('questionnaires')
      table.integer('site_id').unsigned().references('id').inTable('sites')
      table.timestamp('start_at').defaultTo(this.fn.now())
      table.timestamp('end_at').defaultTo(this.fn.now())
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaire_sites')
  }
}

module.exports = QuestionnaireSiteSchema
