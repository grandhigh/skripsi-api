'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionnaireSchema extends Schema {
  up () {
    this.create('questionnaires', (table) => {
      table.string('code', 10).notNullable().unique()
      table.string('name', 80).notNullable()
      table.string('description', 254).notNullable()
      table.string('scale_code').unsigned().references('code').inTable('questionnaire_scales')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('questionnaires')
  }
}

module.exports = QuestionnaireSchema
