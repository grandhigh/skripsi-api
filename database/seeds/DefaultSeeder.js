'use strict'

/*
|--------------------------------------------------------------------------
| DefaultSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Hash = use('Hash')
const Database = use('Database')
const Moment = use('moment')

const User = use('App/Models/User')
const Site = use('App/Models/Site')
const Tag = use('App/Models/Tag')
const SiteTag = use('App/Models/SiteTag')
const ExtConfig = use('App/Models/ExtConfig')
const Questionnaire = use('App/Models/Questionnaire')
const QuestionnaireSite = use('App/Models/QuestionnaireSite')
const QuestionnaireSetup = use('App/Models/QuestionnaireSetup')
const QuestionnaireScale = use('App/Models/QuestionnaireScale')
const QuestionnaireScaleSetup = use('App/Models/QuestionnaireScaleSetup')
const QuestionnaireDimension = use('App/Models/QuestionnaireDimension')

class DefaultSeeder {
	async run () {
		/*Start Transaction*/
		const trx = await Database.beginTransaction()
		try {
			/*Create User*/
			const user = await User.create({
				username: 'admin',
				fullname: 'Admin MTB',
				phone: '081915872532',
				job: 'Programmer',
				email: 'grandhigh003@gmail.com',
				password: await Hash.make('11.2g6aA')
			}, trx)

			/*Create Site*/
			const site = await Site.create({
				name: 'MTB Blog',
				url: 'http://mra-tb.tech',
				description: 'Bersama menuju singularitas',
				created_by: user.id
			}, trx)

			/*Create Config*/
			await ExtConfig.create({
				code: 'defaultSiteId',
				value: site.id,
				description: 'Default Site Id'
			}, trx)

			/*Create Tag*/
			const tags = {
				Programming: 'Pemrograman',
				Tutorials: 'Tutorial',
				Novel: 'Novel',
				News: 'Berita',
				Blog: 'Blog'
			}

			const tag = {}

			for (const name in tags) {
				const description = tags[name]
				tag[name] = await Tag.create({ name, description }, trx)
			}

			/*Create Site Tag*/
			const siteTags = ['Programming', 'Tutorials', 'Blog']
			for (var i = 0; i < siteTags.length; i++) {
				const tag_id = tag[siteTags[i]].id
				const site_id = site.id
				const siteTag = await SiteTag.create({ site_id, tag_id }, trx)
			}

			/*Create Questionnaire Scale*/
			const questionnaireScale = await QuestionnaireScale.create({
				code: '1to5',
				name: 'Skala 1 sampai 5'
			}, trx)

			const scale = ['Sangat Tidak Setuju (STS)', 'Tidak Setuju (TS)', 'Netral (N)', 'Setuju (S)', 'Sangat Setuju (SS)'].reverse()
			for (var i = 0; i < scale.length; i++) {
				const order_no = parseInt(i) + 1
				const scale_code = questionnaireScale.code
				const name = scale[i]
				const value = 5 - parseInt(i)

				await QuestionnaireScaleSetup.create({ scale_code, name, order_no, value }, trx)
			}

			/*Create Questionnaire*/
			const questionnaire = await Questionnaire.create({
				code: 'EUCS',
				name: 'End User Computing Satisfaction',
				description: 'Rancangan Kuesioner EUCS',
				scale_code: questionnaireScale.code
			}, trx)

			/*Create Questionnaire Sie*/
			const questionnaireSite = await QuestionnaireSite.create({
				code: this.makeid(7),
				name: 'Survei MTB dengan Metode EUCS'
				questionnaire_code: questionnaire.code,
				site_id: site.id,
				start_at: Moment().format(),
				end_at: Moment().add(3, 'days').format()
			}, trx)

			/*Create Questionnaire Dimension*/
			const dimensions = { C: 'Content', A: 'Accuracy', F: 'Format', E: 'Ease of Use', T: 'Timeliness' }
			for (const code in dimensions) {
				const questionnaire_code = questionnaire.code
				const name = dimensions[code]
				const order_no = parseInt(Object.keys(dimensions).indexOf(code)) + 1
				const { id: dimension_id } = await QuestionnaireDimension.create({ questionnaire_code, code, name, order_no }, trx)

				const questions = {
					C: [
						'Isi dari informasi pada blog sesuai kebutuhan Anda',
						'Isi dari informasi pada blog mudah dipahami',
						'Isi dari informasi pada blog sudah lengkap',
						'Isi dari informasi pada blog sangat jelas'
					],
					A: [
						'Blog sudah menampilkan informasi yang benar dan akurat',
						'Setiap link pada blog yang Anda klik selalu menampilkan post yang sesuai'
					],
					F: [
						'Desain tampilan blog memiliki pengaturan warna yang menarik',
						'Desain tampilan blog memiliki layout yang memudahkan pengguna',
						'Desain tampilan blog memiliki struktur menu dan link yang mudah dipahami'
					],
					E: [
						'Blog sangat mudah digunakan',
						'Blog mudah diakses dari mana saja dan kapan saja'
					],
					T: [
						'Informasi yang Anda butuhkan dengan cepat diperoleh melalui blog',
						'Blog selalu menampilkan informasi yang terbaru '
					]
				}

				const detail = questions[code]
				for (var i = 0; i < detail.length; i++) {
					const question = detail[i]
					await QuestionnaireSetup.create({ dimension_id, code: `${code}${parseInt(i) + 1}`, question }, trx)
				}
			}

			/*Commit Transaction*/
			await trx.commit()
		} catch (error) {
			/*Rollback Transaction*/
			await trx.rollback()

			/*Throw Error*/
			throw Error(error)
		}
	}

	makeid(length) {
		let result= ''
		const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
		const charactersLength = characters.length
		for ( var i = 0; i < length; i++ ) result += characters.charAt(Math.floor(Math.random() * charactersLength))
		return result
	}
}

module.exports = DefaultSeeder
