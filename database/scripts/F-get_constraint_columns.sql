CREATE OR REPLACE FUNCTION get_constraint_columns(p_table_name VARCHAR, p_constraint_type VARCHAR)
	RETURNS _VARCHAR AS
$BODY$
/**
 * Get Fieign_column
 * @author: Reyhan
 * 
 * Contoh eksekusi: 
 *  SELECT get_constraint_columns('data_details', 'FOREIGN KEY')
 * 
 **/
DECLARE
	v_cols VARCHAR[];
BEGIN
	SELECT
		ARRAY_AGG(kcu.column_name::VARCHAR)
	FROM 
		information_schema.table_constraints AS tc 
		JOIN information_schema.key_column_usage AS kcu
			ON tc.constraint_name = kcu.constraint_name
			AND tc.table_schema = kcu.table_schema
		JOIN information_schema.constraint_column_usage AS ccu
		ON ccu.constraint_name = tc.constraint_name
		AND ccu.table_schema = tc.table_schema
	WHERE tc.constraint_type = p_constraint_type AND tc.table_name = p_table_name INTO v_cols;
	RETURN v_cols;
END; 
$BODY$
	LANGUAGE plpgsql STABLE
	COST 100
