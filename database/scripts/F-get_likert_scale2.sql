CREATE OR REPLACE FUNCTION get_likert_scale(p_site_id INTEGER, p_code VARCHAR, p_detail BOOLEAN)
	RETURNS TABLE(
		variable VARCHAR,
		max_score INTEGER,
		total_respondent INTEGER,
		item INTEGER,
		total_score INTEGER,
		score_kirterium NUMERIC,
		score NUMERIC
	) AS
$BODY$
/**
 * Get Likert Scale
 * @author: Reyhan
 * 
 * Contoh eksekusi: 
 *  SELECT * FROM get_likert_scale(1, 'TEST', true)
 * 
 **/
BEGIN
	RETURN QUERY WITH datum AS (
		WITH xdata2 AS (
			WITH xdata AS (
				SELECT dm.name, qss.value, x2.max_score, x.total_question FROM data d
				INNER JOIN questionnaire_sites qs ON qs.code = d.survey_code
				INNER JOIN data_details dd ON dd.data_id = d.id
				INNER JOIN questionnaire_setups qt ON qt.id = dd.setup_id
				INNER JOIN questionnaire_scale_setups qss ON qss.id = dd.scale_setup_id
				INNER JOIN questionnaire_dimensions dm ON dm.id = qt.dimension_id
				INNER JOIN (
					SELECT qs.code, qs.scale_code, count(*) total_question FROM questionnaires qs
					INNER JOIN questionnaire_dimensions qd ON qd.questionnaire_code = qs.code
					INNER JOIN questionnaire_setups qst ON qst.dimension_id = qd.id GROUP BY 1, 2
				) x ON x.code = qs.questionnaire_code
				INNER JOIN (
					SELECT qc.scale_code, MAX(qc.value) max_score FROM questionnaire_scale_setups qc GROUP BY 1
				) x2 ON x2.scale_code = x.scale_code
				WHERE qs.site_id = 1 AND d.survey_code = 'test' ORDER BY 1
			) SELECT *, (SELECT COUNT(*) FROM xdata) / total_question total_respondent FROM xdata
		) SELECT
			x2.name, x2.max_score, x2.total_respondent, count(*) / x2.total_respondent item, SUM(value) total_score
		FROM xdata2 x2
		GROUP BY 1, 2, 3
	) SELECT
		d.name, d.max_score, d.total_respondent::INTEGER, d.item::INTEGER, d.total_score::INTEGER, (d.max_score * d.total_respondent * d.item)::FLOAT::NUMERIC score_kirterium,
		ROUND(((d.total_score::FLOAT / (d.max_score * d.total_respondent * d.item)::FLOAT) * 100)::NUMERIC, 2)::FLOAT::NUMERIC score
	FROM datum d;
END; 
$BODY$
	LANGUAGE plpgsql STABLE
	COST 100
	ROWS 1000;
