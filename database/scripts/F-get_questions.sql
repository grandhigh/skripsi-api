CREATE OR REPLACE FUNCTION get_qeustions(p_code VARCHAR)
	RETURNS TABLE(
		id INTEGER,
		parent_code VARCHAR,
		parent_name VARCHAR,
		question VARCHAR,
		scales JSON
	) AS
$BODY$
/**
 * Get Questions
 * @author: Reyhan
 * 
 * Contoh eksekusi: 
 *  SELECT * FROM get_qeustions('EUCS')
 * 
 **/
BEGIN
	RETURN QUERY SELECT s.id, d.code parent_code, d.name parent_name, s.question, array_to_json(array_agg(y.*)) scales FROM questionnaire_sites z
	INNER JOIN questionnaires q ON q.code = z.questionnaire_code
	INNER JOIN questionnaire_dimensions d ON d.questionnaire_code = q.code
	INNER JOIN questionnaire_setups s ON s.dimension_id = d.id
	INNER JOIN questionnaire_scales x ON x.code = q.scale_code
	INNER JOIN questionnaire_scale_setups y ON y.scale_code = x.code
	WHERE z.code = p_code
	GROUP BY 1, 2, 3, 4, d.order_no, s.code ORDER BY d.order_no, s.code;
END; 
$BODY$
	LANGUAGE plpgsql STABLE
	COST 100
	ROWS 1000;
